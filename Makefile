PROG=		main
MAN=
CFLAGS+=	-I/usr/local/include -O0 -ggdb \
		-Iinclude/
LDADD+=		-L/usr/local/lib -lsqlite3
LIBADD+=	pthread

SRCS=		src/main.c \
		src/message_queue.c \
		src/connection.c \
		src/receiver.c \
		src/relay.c

.include <bsd.prog.mk>
