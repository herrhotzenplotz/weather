/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <weather/relay.h>

#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

static
void *relay_worker(void *_relay)
{
    relay *relay = _relay;

    for (;;)
    {
        struct sockaddr remote_addr;
        socklen_t remote_addr_len;

        // TODO: Save remote addr into the connection
        int remote_fd = accept(relay->socket_fd, &remote_addr, &remote_addr_len);

        if (remote_fd < 0)
        {
            fprintf(stderr, "ERR  : Unable to accept connection\n");
            continue;
        }

        fprintf(stdout, "INFO : Accepted incoming connection\n");

        connection *connection = NULL;
        for (size_t i = 0; i < CONNECTION_LIST_MAX_CONNECTIONS; ++i)
        {
            if (!relay->connection_list->connections[i].is_open)
            {
                connection = &relay->connection_list->connections[i];
                break;
            }
        }

        if (!connection)
        {
            fprintf(stderr, "ERR  : No space to allocate connection\n");
            close(remote_fd);
        }

        connection_create(connection, remote_fd);
    }

    return NULL;
}

void relay_start(relay *relay)
{
    relay->socket_fd = socket(PF_INET, SOCK_STREAM, 0);
    if (relay->socket_fd < 0)
    {
        fprintf(stderr, "ERR  : socket() failed\n");
        abort();
    }

    struct addrinfo hint = {0}, *res;
    hint.ai_family       = AF_INET;
    hint.ai_socktype     = SOCK_STREAM;
    hint.ai_protocol     = IPPROTO_TCP;
    hint.ai_flags        = AI_PASSIVE;

    if (getaddrinfo(NULL, "6970", &hint, &res) != 0)
    {
        fprintf(stderr, "ERR  : getaddrinfo() failed: %s\n", strerror(errno));
        abort();
    }

    if (bind(relay->socket_fd, (const struct sockaddr*)res->ai_addr,
             res->ai_addrlen) < 0)
    {
        fprintf(stderr, "ERR  : bind() failed: %s\n", strerror(errno));
        abort();
    }

    if (listen(relay->socket_fd, 6) < 0)
    {
        fprintf(stderr, "ERR  : listen() failed: %s\n", strerror(errno));
        abort();
    }

    int err = pthread_create(&relay->worker_thread, NULL,
                             relay_worker, relay);
    if (err)
    {
        fprintf(stderr, "ERR  : Unable to spawn relay worker thread: %s\n",
                strerror(err));
        abort();
    }
}
