/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <weather/connection.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

static
void *connection_worker(void *_connection)
{
    connection *connection = _connection;
    int err;

    err = pthread_mutex_lock(&connection->outgoing_queue.queue_lock);
    if (err)
    {
        fprintf(stderr, "ERR  : Unable to lock message queue: %s\n",
                strerror(err));
        pthread_exit(NULL);
    }

    while (connection->is_open)
    {
        string message = message_queue_dq(&connection->outgoing_queue);
        int bytes_written = 0;

        do {
            int n_bytes = send(connection->socket_fd,
                               message.data + bytes_written,
                               message.length - bytes_written,
                               MSG_NOSIGNAL);
            if (n_bytes < 0)
            {
                connection->is_open = 0;
                goto close;
            }

            bytes_written += n_bytes;
        } while (bytes_written < message.length);

        free(message.data);
    }

    err = shutdown(connection->socket_fd,
                   SHUT_RDWR);
    if (err)
    {
        fprintf(stderr, "ERR  : Unable to shutdown socket: %s\n",
                strerror(errno));
        goto exit;
    }

close:
    err = close(connection->socket_fd);
    if (err)
    {
        fprintf(stderr, "ERR  : Unable to close socket: %s\n",
                strerror(errno));
        goto exit;
    }

exit:
    fprintf(stdout, "INFO : Disconnected from relay station. Exiting thread...\n");
    pthread_exit(NULL);
}

void connection_create(connection *connection, int socket_fd)
{
    int err;

    connection->socket_fd = socket_fd;
    message_queue_init(&connection->outgoing_queue);

    err = pthread_create(&connection->worker_thread, NULL,
                         connection_worker, connection);

    if (err)
    {
        fprintf(stderr, "ERR  : Unable to spawn connection worker: %s\n",
                strerror(err));
        abort();
    }

    connection->is_open = 1;
}
