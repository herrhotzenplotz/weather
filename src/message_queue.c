/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
l * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <weather/message_queue.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void message_queue_init(message_queue *q)
{
    int err;

    err = pthread_mutex_init(&q->queue_lock, NULL);
    if (err)
    {
        fprintf(stderr, "Unable to init mutex: %s\n",
                strerror(err));
        abort();
    }

    err = pthread_cond_init(&q->queue_signal, NULL);
    if (err)
    {
        fprintf(stderr, "Unable to init condition variable: %s\n",
                strerror(err));
        abort();
    }
}

void message_queue_destroy(message_queue *q)
{
    pthread_mutex_destroy(&q->queue_lock);
    pthread_cond_destroy(&q->queue_signal);
}

void message_queue_nq(message_queue *q, string m)
{
    int err = pthread_mutex_lock(&q->queue_lock);
    if (err)
    {
        fprintf(stderr, "Unable to lock the queue: %s\n",
                strerror(err));
        abort();
    }

    int idx = (q->begin + q->size) % MESSAGE_QUEUE_CAPACITY;
    q->buffer[idx] = m;

    if (q->size == MESSAGE_QUEUE_CAPACITY)
    {
        q->begin = (q->begin + 1) % MESSAGE_QUEUE_CAPACITY;
    }
    else
    {
        q->size += 1;
    }

    err = pthread_cond_signal(&q->queue_signal);
    if (err)
    {
        fprintf(stderr, "unable to signal queue: %s\n",
                strerror(err));
        abort();
    }

    err = pthread_mutex_unlock(&q->queue_lock);
    if (err)
    {
        fprintf(stderr, "unable to unlock queue: %s\n",
                strerror(err));
    }
}

string message_queue_dq(message_queue *q)
{
    int err;

    while (q->size <= 0)
    {
        err = pthread_cond_wait(&q->queue_signal, &q->queue_lock);
        if (err)
        {
            fprintf(stderr, "ERR  : Unable to wait for queue signal: %s\n",
                    strerror(err));
            abort();
        }
    }

    q->size -= 1;
    int idx = q->begin;
    q->begin = (q->begin + 1) % MESSAGE_QUEUE_CAPACITY;
    return q->buffer[idx];
}
