/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <weather/receiver.h>

char insert_sql[] =
    "INSERT INTO WeatherData (weatherTemperature, "
    "weatherHumidity, weatherPressure)"
    "VALUES (:temperature, :humidity, :pressure);";

static
void *receiver_worker(void *_receiver)
{
    receiver *r = _receiver;

    int sql_err;
    sqlite3_stmt *stmt;

    sql_err = sqlite3_prepare_v2(
        r->db, insert_sql,
        sizeof(insert_sql),
        &stmt, NULL);
    if (sql_err != SQLITE_OK)
    {
        fprintf(stderr, "ERR  : Unable to prepare statement: %s\n",
                sqlite3_errmsg(r->db));
        pthread_exit(NULL);
    }


    for (;;)
    {
        char buffer[64];
        ssize_t n;

        struct sockaddr client = {0};
        socklen_t client_len = sizeof(client);

        n = recvfrom(r->socket_fd, buffer, sizeof(buffer), 0,
                     &client, &client_len);

        if (r->is_first_connection)
        {
            memcpy(&r->first_client, &client, client_len);
            r->is_first_connection = 0;
        }
        else
        {
            if (
                ((struct sockaddr_in *)(&client))->sin_addr.s_addr
                != ((struct sockaddr_in *)(&r->first_client))->sin_addr.s_addr)
            {
                fprintf(stderr, "WARN : Skipping message from unexpected client\n");
                continue;
            }
        }

        if (n == -1)
        {
            fprintf(stderr, "ERR  : recv() failed!\n");
            pthread_exit(NULL);
        }

        char *begin_ptr = buffer;

        for (size_t i = 0; i < 3; ++i)
        {
            char *endptr;
            double value = strtod(begin_ptr, &endptr);

            if (endptr == NULL)
            {
                break;
            }

            if ((*endptr != '\n') && (*endptr != '\t'))
            {
                fprintf(stderr, "ERR  : Unexpected endptr\n");
                pthread_exit(NULL);
            }

            sql_err = sqlite3_bind_double(stmt, i + 1, value);
            if (sql_err != SQLITE_OK)
            {
                fprintf(stderr, "ERR  : Shit fuck damn: %s\n",
                        sqlite3_errmsg(r->db));
                pthread_exit(NULL);
            }

            begin_ptr = endptr + 1;
        }

        sql_err = sqlite3_step(stmt);
        if (sql_err != SQLITE_DONE)
        {
            fprintf(stderr, "ERR  : Unable to step statement: %s\n",
                    sqlite3_errmsg(r->db));
            abort();
        }

        sql_err = sqlite3_reset(stmt);
        if (sql_err != SQLITE_OK)
        {
            fprintf(stderr, "ERR  : Cannot reeeeeset the statement: %s\n",
                    sqlite3_errmsg(r->db));
        }

        for (size_t i = 0; i < CONNECTION_LIST_MAX_CONNECTIONS; ++i)
        {
            if (r->connection_list->connections[i].is_open)
            {
                char *copy = malloc(n * sizeof(char));
                memcpy(copy, buffer, n);
                string outgoing_message = (string)
                {
                    .data   = copy,
                    .length = n
                };

                message_queue_nq(
                    &r->connection_list->connections[i].outgoing_queue,
                    outgoing_message);
            }
        }


        printf("INFO : Inserted and relayed %.*s", (int)n, buffer);
    }

    return NULL;
}

void receiver_start(receiver *receiver)
{
    int err;

    err = pthread_create(&receiver->worker_thread, NULL,
                         receiver_worker, receiver);
    if (err)
    {
        fprintf(stderr, "ERR  : Unable to spawn receive worker thread: %s\n",
                strerror(err));
        abort();
    }
}
