/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <sqlite3.h>

#include <weather/message_queue.h>
#include <weather/receiver.h>
#include <weather/relay.h>

char create_table_sql[] =
    "CREATE TABLE IF NOT EXISTS WeatherData ("
    "id INTEGER PRIMARY KEY,"
    "weatherTemperature REAL NOT NULL,"
    "weatherHumidity REAL NOT NULL,"
    "weatherPressure REAL NOT NULL,"
    "weatherTime DATETIME NOT NULL DEFAULT (datetime('now'))"
    ");";

int main(int argc, char *argv[])
{
    int socket_fd;

    sqlite3 *db;
    sqlite3_stmt *stmt;

    if (argc != 2)
    {
        fprintf(stderr, "ERR  : usage: %s database.db\n",
                argv[0]);
        abort();
    }

    if (sqlite3_open(argv[1], &db) != SQLITE_OK)
    {
        fprintf(stderr, "ERR  : Cannot open database: %s\n",
                sqlite3_errmsg(db));
        abort();
    }

    char *errmsg;
    if (sqlite3_exec(db, create_table_sql, NULL, NULL, &errmsg) != SQLITE_OK)
    {
        fprintf(stderr, "ERR  : Cannot sqlite_exec(): %s\n",
                errmsg);
        abort();
    }

    socket_fd = socket(PF_INET, SOCK_DGRAM, 0);

    if (socket_fd < 0)
    {
        fprintf(stderr, "ERR  : socket() failed\n");
        abort();
    }

    struct addrinfo hint = {0}, *res;
    hint.ai_family       = AF_INET;
    hint.ai_socktype     = SOCK_DGRAM;
    hint.ai_protocol     = IPPROTO_UDP;
    hint.ai_flags        = AI_PASSIVE;

    if (getaddrinfo(NULL, "6969", &hint, &res) != 0)
    {
        fprintf(stderr, "ERR  : getaddrinfo() failed: %s\n", strerror(errno));
        abort();
    }

    if (bind(socket_fd, (const struct sockaddr*)res->ai_addr,
             res->ai_addrlen) < 0)
    {
        fprintf(stderr, "ERR  : bind() failed: %s\n", strerror(errno));
        abort();
    }

    connection_list connections = {0};
    receiver receiver = {0};
    receiver.socket_fd = socket_fd;
    receiver.db = db;
    receiver.connection_list = &connections;
    receiver.is_first_connection = 1;

    receiver_start(&receiver);

    relay relay = {0};
    relay.connection_list = &connections;
    relay_start(&relay);

    pthread_join(receiver.worker_thread, NULL);
    pthread_join(relay.worker_thread, NULL);

    sqlite3_close(db);

    return 0;
}
